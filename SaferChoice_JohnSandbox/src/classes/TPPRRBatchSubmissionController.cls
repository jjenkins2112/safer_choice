public with sharing class TPPRRBatchSubmissionController {

    private TPP_RR_Batch__c tppRRBatch;
    public boolean allEligible {get;set;}
    public boolean addNew {get;set;}
    public boolean addExisting {get;set;}
    
    public Submission_Metric__c sm {get;set;}
    
    public id holderID {get;set;}
    
    public TPPRRBatchSubmissionController(ApexPages.standardController stdController){
        tppRRBatch = (TPP_RR_Batch__C)stdController.getRecord();
        
        sm = new Submission_Metric__c();
        
        allEligible = false;
        addNew = false;
        addExisting = false;
    }
    
    public List<Batch> getTPPRRs(){
        List<Batch> tpprrs = new List<Batch>();
        for (TPP_Review_Recomendation__c tppRR : [select id, Name from TPP_Review_recomendation__c where TPP_RR_Batch__c = :tppRRBatch.ID]){
            tppRRs.add(new Batch(tppRR));
        }
        
        for (Batch tpprr : tppRRs){
            if (tppRR.eligible) allEligible = true;
            else{
                allEligible = false;
                break;
            }
        }
        return tpprrs;
    }
    
    public PageReference addNewTPPRR(){
        addNew = true;
        return null;
    }
    
    public PageReference addExistingTPPRR(){
        addExisting = true;
        return null;
    }
    
    public PAgeReference saveTPPRR(){
		if(sm.TPP_Review_Recommendation__c != null){
			update new TPP_Review_Recomendation__c(id = sm.TPP_Review_Recommendation__c, TPP_RR_Batch__c = tppRRBatch.ID);
			sm = new Submission_Metric__c();
			addNew = false;
        }
        return null;
    }
    
    public PageReference submitAll(){
        Map<id, TPP_Review_Recomendation__c> tppRevRecMap = new Map<id, TPP_Review_Recomendation__c>([select id from TPP_Review_Recomendation__c where TPP_RR_Batch__c = :tppRRBatch.ID]);
        ButtonMethods.submitReviews(tppRevRecMap.keyset());
        return null;
    }
    
    public class Batch{
        public TPP_Review_Recomendation__c tppRR {get;set;}
        public boolean eligible {get;set;}
        
        public Batch(TPP_Review_Recomendation__c tppRR){
            this.eligible = isEligible(tppRR.ID);
            this.tppRR = tppRR;
        }
        
    }
    
    private static boolean isEligible(ID revRecID){
        boolean reviewRecReady = false;
        
        string query = UtilityMethods.buildQueryAllString('TPP_Review_Recomendation__c', 'id =\'' + revRecID + '\'');
        query = query.replace('SELECT', 'SELECT Formulation__r.Name, ');
        TPP_Review_Recomendation__c revRec = Database.query(query);
        
        //Get RecordType Id's
        String rtTPPInitialReviewFullProduct = Util.getRecordTypeId('TPP_Review_Recomendation__c', 'Initial_Review_Full_Product');
        String rtTPPRenewal = Util.getRecordTypeId('TPP_Review_Recomendation__c', 'Renewal');
        String rtTPPInitialReviewFormulation = Util.getRecordTypeId('TPP_Review_Recomendation__c', 'Initial_Review_Formulation');
        String rtTPPReformulation = Util.getRecordTypeId('TPP_Review_Recomendation__c', 'Reformulation');
        String rtTPPCleanGredients = Util.getRecordTypeId('TPP_Review_Recomendation__c', 'CleanGredients');
         List<Attachment> attachmentPackagingSustainability = new List<Attachment>();
            List<Attachment> attachmentPackagingIngredients = new List<Attachment>();
            List<Attachment> attachmentIngredientDisclosure = new List<Attachment>();
            List<Attachment> attachmentPerformance = new List<Attachment>();
            List<Attachment> attachmentMSDS = new List<Attachment>();
            List<Attachment> attachmentProductLabel = new List<Attachment>();
    
        for (Attachment a : [select id, Name, Body, ContentType from Attachment where PArentID = :revRec.ID order by LastModifiedDate Desc]){
            if ((attachmentPackagingSustainability == null || attachmentPackagingSustainability.size() == 0) && a.Name.startsWith('Packaging-Sustainability')) attachmentPackagingSustainability = new List<Attachment>{a};
            else if ((attachmentPackagingIngredients == null || attachmentPackagingIngredients.size() == 0) && a.Name.startsWith('Packaging-Ingredients')) attachmentPackagingIngredients = new List<Attachment>{a};
            else if ((attachmentIngredientDisclosure == null || attachmentIngredientDisclosure.size() == 0) && a.Name.startsWith('Ingredient-Disclosure')) attachmentIngredientDisclosure =new List<Attachment>{a};
            else if ((attachmentPerformance == null || attachmentPerformance.size() == 0) && a.Name.startsWith('Performance')) attachmentPerformance = new List<Attachment>{a};
            else if ((attachmentMSDS == null || attachmentMSDS.size() == 0) && (a.Name.startsWith('MSDS') || a.Name.startsWith('SDS'))) attachmentMSDS = new List<Attachment>{a};
            else if ((attachmentProductLabel == null || attachmentProductLabel.size() == 0) && a.Name.startsWith('Product-Label')) attachmentProductLabel = new List<Attachment>{a};
        }

        if (attachmentPackagingSustainability == null) attachmentPackagingSustainability = new List<Attachment>();
        if (attachmentPackagingIngredients == null) attachmentPackagingIngredients = new List<Attachment>();
        if (attachmentIngredientDisclosure == null) attachmentIngredientDisclosure = new List<Attachment>();
        if (attachmentPerformance == null) attachmentPerformance = new List<Attachment>();
        if (attachmentMSDS == null) attachmentMSDS = new List<Attachment>();
        if (attachmentProductLabel == null) attachmentProductLabel = new List<Attachment>();

        // For each different review record type, there are different required fields and files. For each record type, check the required fields and files. 
        if (revRec.RecordTypeId == rtTPPInitialReviewFullProduct) {
            // Initial Review - Full Product
            // Check for all required fields being entered on this review
            if (revRec.Overall_Recommendation__c != null
                        && revRec.Overall_Recommendation_Notes__c != null
                        && revRec.pH__c != null
                        && revRec.pH_Notes__c != null
                        && revRec.VOCs__c != null
                        && revRec.VOCs_Notes__c != null
                        && revRec.Flammability__c != null
                        && revRec.Flammability_Notes__c != null
                        && revRec.Performance__c != null
                        && revRec.Performance_Notes__c != null
                        && revRec.Primary_Packaging__c != null
                        && revRec.Primary_Packaging_Notes__c != null
                        && revRec.Ingredient_Disclosure__c != null
                        && revRec.Ingredient_Disclosure_Method__c != null
                        && revRec.Ingredient_Disclosure_Notes__c != null
                        && revRec.Total_of_target_for_improvement_ingredie__c != null
                        && revRec.Target_for_improvement_Ingredients_Notes__c != null
                        && revRec.Ingredients_of_Concern__c != null
                        && revRec.Ingredients_of_Concern_Notes__c != null
                        && revRec.Logo__c != null
                        && revRec.Logo_Notes__c != null
                        && revRec.Environmental_Marketing_Claims__c != null 
                        && revRec.Environmental_Marketing_Claims_Notes__c != null
                        && revRec.Endorsement_Disclaimer__c != null
                        && revRec.Endorsement_Disclaimer_Notes__c != null
                        /*&& revRec.Flagged_Chemicals__c != null
                        && revRec.Flagged_Chemical_Notes__C != null*/) {
                reviewRecReady = true;
            } 
            
            // If all the fields are set, check for the required files
            if (reviewRecReady == true) {
                // Check for required files
                if (attachmentPackagingSustainability.size() < 1 ||
                    attachmentPackagingIngredients.size() < 1 ||
                    attachmentIngredientDisclosure.size() < 1 ||
                    attachmentPerformance.size() < 1 ||
                    attachmentMSDS.size() < 1 ||
                    attachmentProductLabel.size() < 1){
                    reviewRecReady = false;
                }
            } 
        } 
        else if (revRec.RecordTypeId == rtTPPRenewal) {
            // Renewal type
            // Check for all required fields being entered on this review
            if (revRec.Overall_Recommendation__c != null
                        && revRec.Overall_Recommendation_Notes__c != null
                        && revRec.pH__c != null
                        && revRec.pH_Notes__c != null
                        && revRec.VOCs__c != null
                        && revRec.VOCs_Notes__c != null
                        && revRec.Flammability__c != null
                        && revRec.Flammability_Notes__c != null
                        && revRec.Performance__c != null
                        && revRec.Performance_Notes__c != null
                        && revRec.Primary_Packaging__c != null
                        && revRec.Primary_Packaging_Notes__c != null
                        && revRec.Ingredient_Disclosure__c != null
                        && revRec.Ingredient_Disclosure_Method__c != null
                        && revRec.Ingredient_Disclosure_Notes__c != null
                        && revRec.Total_of_target_for_improvement_ingredie__c != null
                        && revRec.Target_for_improvement_Ingredients_Notes__c != null
                        && revRec.Alts_to_target_for_improvement__c != null
                        && revRec.Alts_to_target_for_improvement_Notes__c != null
                        && revRec.Alts_to_Green_Chem_Challenge__c != null
                        && revRec.Alts_to_Green_Chem_Challenge_Notes__c != null
                        && revRec.Ingredients_of_Concern__c != null
                        && revRec.Ingredients_of_Concern_Notes__c != null
                        && revRec.Logo__c != null
                        && revRec.Logo_Notes__c != null
                        && revRec.Environmental_Marketing_Claims__c != null
                        && revRec.Environmental_Marketing_Claims_Notes__c != null
                        && revRec.Endorsement_Disclaimer__c != null
                        && revRec.Endorsement_Disclaimer_Notes__c != null) {
                reviewRecReady = true;
            } 
            
            // If all the fields are set, check for the required files
            if (reviewRecReady == true) {
                // Check for required files
                // Packaging Sustainability 
                if (attachmentPackagingSustainability.size() < 1 ||
                        attachmentPackagingIngredients.size() < 1 ||
                        attachmentIngredientDisclosure.size() < 1 ||
                        attachmentPerformance.size() < 1 ||
                        attachmentMSDS.size() < 1 ||
                        attachmentProductLabel.size() < 1) {
                    reviewRecReady = false;
                }
            }
        }
        else if (revRec.RecordTypeId == rtTPPInitialReviewFormulation) {
            // Initial Review � Formulation type
            // Check for all required fields being entered on this review
            if (revRec.Overall_Recommendation__c != null
                        && revRec.Overall_Recommendation_Notes__c != null
                        && revRec.pH__c != null
                        && revRec.pH_Notes__c != null
                        && revRec.VOCs__c != null
                        && revRec.VOCs_Notes__c != null
                        && revRec.Flammability__c != null
                        && revRec.Flammability_Notes__c != null
                        && revRec.Ingredient_Disclosure__c != null
                        && revRec.Ingredient_Disclosure_Method__c != null
                        && revRec.Ingredient_Disclosure_Notes__c != null
                        && revRec.Total_of_target_for_improvement_ingredie__c != null
                        && revRec.Target_for_improvement_Ingredients_Notes__c != null) {
                reviewRecReady = true;
            } 
            
            // If all the fields are set, check for the required files
            if (reviewRecReady == true) {
                // Check for required files
                if (attachmentIngredientDisclosure.size() < 1) {
                    reviewRecReady = false;
                } 
            } 
        } 
        else if (revRec.RecordTypeId == rtTPPReformulation) {
            // Reformulation type
            // Check for all required fields being entered on this review
            if (revRec.Overall_Recommendation__c != null
                        && revRec.Overall_Recommendation_Notes__c != null
                        && revRec.pH__c != null
                        && revRec.pH_Notes__c != null
                        && revRec.VOCs__c != null
                        && revRec.VOCs_Notes__c != null
                        && revRec.Flammability__c != null
                        && revRec.Flammability_Notes__c != null
                        && revRec.Performance__c != null
                        && revRec.Performance_Notes__c != null
                        && revRec.Ingredient_Disclosure__c != null
                        && revRec.Ingredient_Disclosure_Method__c != null
                        && revRec.Ingredient_Disclosure_Notes__c != null
                        && revRec.Total_of_target_for_improvement_ingredie__c != null
                        && revRec.Target_for_improvement_Ingredients_Notes__c != null) {
                reviewRecReady = true;
            } 
            
            // If all the fields are set, check for the required files
            if (reviewRecReady == true) {
                // Check for required files
                // Ingredient Disclosure 
                if (attachmentIngredientDisclosure.size() < 1 ||
                    attachmentPerformance.size() < 1 ||
                    attachmentMSDS.size() < 1 ||
                    attachmentProductLabel.size() < 1) {
                    reviewRecReady = false;
                } 
            } 
        } 
        else if (revRec.RecordTypeId == rtTPPCleanGredients) {
            // CleanGredients type
            // Check for all required fields being entered on this review
            if (revRec.Overall_Recommendation__c != null
                        && revRec.Ingredient__c != null
                        && revRec.Data_on_New_Chemicals_Provided__c != null
                        && revRec.Data_on_New_Chemicals_Provided_Notes__c != null
                        && revRec.Yellow_Triangle_Ingredients_10__c != null
                        && revRec.Yellow_Triangle_Ingredients_10_Notes__c != null
                        && revRec.Residuals_of_Concern_0_01__c != null
                        && revRec.Residuals_of_Concern_0_01_Notes__c != null
                        && revRec.Acceptable_for_Direct_Release__c != null
                        && revRec.Acceptable_for_Direct_Release_Notes__c != null) {
                reviewRecReady = true;
            } 
            
            // If all the fields are set, check for the required files
            if (reviewRecReady == true) {
                // Check for required files
                if (attachmentMSDS.size() < 1) {
                    reviewRecReady = false;
                } 
            } 
        }

        boolean reviewCheck = true;
        //formulation check
        if (revRec.RecordTypeId != rtTPPCleanGredients){
            //formulation must be attached to product
            if ([select count() from Relationship_Formulation_Product__c where Formulation__c = :revRec.Formulation__c] == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'The formulation attached to this review ('+revRec.Formulation__r.Name+') needs to be attached to a product.'));

                reviewCheck = false;
            }

            Set<id> ingredientIDs = new Set<id>();
            //require fields for formulation components
            map<string,list<string>> rcifMap = new map<string,list<string>>();
            map<string,string> rcifNameIDMap = new map<string,string>();
            
            for (Relationship_Chem_Ing_Formulation__c rcif : [select id,
                                                                    Name,
            														Chemical__r.Name,
            														Chemical__r.EO__c,
            														Minimum_Number_of_EOs__c,
                                                                    Number_of_EOs__c,
            														Chemical__r.PO__c,
            														Minimum_Number_of_POs__c,
            														Number_of_POs__c,
            														Average_MW__c,
            														Number_of_repeating_units__c,
            														Percent_MW_500__c,
            														Percent_MW_1_000__c,
                                                                    Min_Percent_Composition__c,
                                                                    Max_Percent_Composition__c,
                                                                    Component_Class__c,
                                                                    Account__c,
                                                                    TPP_Status__c,
                                                                    TPP_Status_Comments__c,
                                                                    Ingredient__c 
                                                                    from Relationship_Chem_Ing_Formulation__c where Formulation__c =:revRec.Formulation__c]){
                list<string> rcifNames = new list<string>();
				rcifNameIDMap.put(rcif.Name,rcif.id);
				
				if(rcif.Min_Percent_Composition__c == null){
					rcifNames.add('Min Percent Composition');
					reviewCheck = false;
				}
				if(rcif.Max_Percent_Composition__c == null){
					rcifNames.add('Max Percent Composition');
					reviewCheck = false;
				}
				if(rcif.Component_Class__c == null){
					rcifNames.add('Component Class');
					reviewCheck = false;
				}
				if(rcif.Account__c == null){
					rcifNames.add('Supplier');
					reviewCheck = false;
				}
				if(rcif.TPP_Status__c == null){
					rcifNames.add('TPP Status');
					reviewCheck = false;
				}
				if(rcif.TPP_Status_Comments__c == null){
					rcifNames.add('TPP Status Comments');
					reviewCheck = false;
				}
				
				if(rcifNames.size()>0){
					rcifMap.put(rcif.Name,rcifNames);
				}
				
                if (rcif.Ingredient__c != null && reviewCheck) ingredientIDs.add(rcif.Ingredient__c);
            }
            
            for(string rName : rcifMap.keyset()){
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'A formulation component <a href="/'+rcifNameIDMap.get(rName)+'" target="_blank">'+rName+'</a> is missing required field(s) '+rcifMap.get(rname)+'.'));                	
            }
            
            for (Ingredient__c ing : [select id, Name, (Select id from Ingredient_Formulations__r) from Ingredient__c where id in :ingredientIDs]){
                if (ing.Ingredient_Formulations__r == null || ing.Ingredient_Formulations__r.size() == 0){
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'An ingredient <a href="/'+ing.id+'" target="_blank">'+ing.name+'</a> contained in the attached formulation does not have an attached ingredient formulation.'));
                    reviewCheck = false;
                }
            }

            AggregateResult ar = [select sum(Min_Percent_Composition__c) min, sum(Max_Percent_Composition__c) max from Relationship_Chem_Ing_Formulation__c where Alternate_Supplier__c = false and Formulation__c = :revRec.Formulation__c];
            
            if ((Decimal)ar.get('min') > 100 || (Decimal)ar.get('max') < 100){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'The range of total min percent composition to total max percent composition for all non-alternate formulation components must contain 100.'));

                reviewCheck = false;
            }
        
        }
        else{
            //require TPP status and TPP Status comments for ingredients
            if (revRec.Ingredient_formulation__c != null){
                
                Ingredient_Formulation__c relatedIng = [select ID, Name, Class__c, Supplier__c, TPP_Status__c, TPP_Last_Review_Date__c, TPP_Review_Notes__c from Ingredient_Formulation__c where id = :revRec.Ingredient_Formulation__c];
         		
         		string ingFrmErrorMsg = '';
         		
                if (relatedIng.Class__c == null){
                    reviewCheck = false;
                    ingFrmErrorMsg = 'Class';
                }
                if (relatedIng.Supplier__c == null){
                    reviewCheck = false;
                    if(ingFrmErrorMsg == ''){ingFrmErrorMsg='Supplier';}
                    else{ingFrmErrorMsg=ingFrmErrorMsg+', Supplier';}
                }
                if (relatedIng.TPP_Status__c == null){
                    reviewCheck = false;
                    if(ingFrmErrorMsg == ''){ingFrmErrorMsg='TPP Status';}
                    else{ingFrmErrorMsg=ingFrmErrorMsg+', TPP Status';}
                }
                if (relatedIng.TPP_Last_Review_Date__c == null){
                    reviewCheck = false;
                    if(ingFrmErrorMsg == ''){ingFrmErrorMsg='TPP Last Review Date';}
                    else{ingFrmErrorMsg=ingFrmErrorMsg+', TPP Last Review Date';}
                }
                if (relatedIng.TPP_Review_Notes__c == null){
                    reviewCheck = false;
                    if(ingFrmErrorMsg == ''){ingFrmErrorMsg='TPP Review Notes';}
                    else{ingFrmErrorMsg=ingFrmErrorMsg+', TPP Review Notes';}
                }
                
                if(ingFrmErrorMsg != ''){
                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'The linked ingredient formulation <a href="/'+relatedIng.ID+'" target="_blank">('+relatedIng.Name+')</a> must have its '+ingFrmErrorMsg+' populated.'));
                }
                
                set<id> ingredientIDs = new Set<id>();

                //require fields for ingredient components
                map<string, string> rciiNameID = new map<string, string>();                
                map<String, list<String>> rciiNamesMap = new map<String, list<String>>(); 
                
                for (Relationship_Chem_Ing_Ing__c rcii : [select id,
                                                                    Name,
            														Chemical__r.Name,
            														Chemical__r.ID,
            														Chemical__r.EO__c,
            														Degree_of_Branching__c,
            														Description_of_Branching__c,
            														Minimum_Number_of_EOs__c,
                                                                    Number_of_EOs__c,
            														Chemical__r.PO__c,
            														Minimum_Number_of_POs__c,
            														Number_of_POs__c,
                                                                    Min_Percent_Composition__c,
                                                                    Max_Percent_Composition__c,
                                                                    Component_Class__c,
                                                                    Supplier__c,
                                                                    TPP_Status2__c,
                                                                    TPP_Status_Comments__c,
                                                                    Component_Ingredient__c, 
                                                                    Average_MW__c,
                                                                    Number_of_repeating_units__c,
                                                                    Percent_MW_500__c,
                                                                    Percent_MW_1_000__c,
                                                                    Monomer_Ratio__c,
                                                                    Is_this_a_block_co_polymer__c,
                                                                    Polymer_Soluble_Dispersable_Swellable__c,
                                                                    Source_ID__c,
                                                                    Residual_Microorganism_Level__c,
                                                                    Is_this_enzyme_encapsulated__c 
                                                                    from Relationship_Chem_Ing_Ing__c where Ingredient_Formulation__c =:revRec.Ingredient_Formulation__c]){            
					rciiNameID.put(rcii.Name,rcii.ID);
          			list<string> rciiFields = new list<string>();
          			
          			if(rcii.Min_Percent_Composition__c == null){
          				rciiFields.add('Min Percent Composition');
          				reviewCheck = false;
          			}
          			if(rcii.Max_Percent_Composition__c == null){
          				rciiFields.add('Max Percent Composition');
          				reviewCheck = false;
          			}          
          			if(rcii.Component_Class__c == null){
          				rciiFields.add('Component Class');
          				reviewCheck = false;
          			}  
          			if(rcii.Supplier__c == null){
          				rciiFields.add('Supplier');
          				reviewCheck = false;
          			}
          			if(rcii.TPP_Status2__c == null){
          				rciiFields.add('TPP Status');
          				reviewCheck = false;
          			}
          			if(rcii.TPP_Status_Comments__c == null){
          				rciiFields.add('TPP Status Comments');
          				reviewCheck = false;
          			}
          			
					if (rcii.Component_Class__c == 'Surfactant') {
						if(rcii.Degree_of_Branching__c == null){
							rciiFields.add('Degree of Branching');
							reviewCheck = false;
						}
						if(rcii.Description_of_Branching__c == null){
							rciiFields.add('Description of Branching');
							reviewCheck = false;
						}
						if(rcii.chemical__r.EO__c){
							if(rcii.Minimum_Number_of_EOs__c == null){
								rciiFields.add('Minimum Number of EOs');
								reviewCheck = false;
							}
							if(rcii.Number_of_EOs__c == null){
								rciiFields.add('Maximum Number of EOs');
								reviewCheck = false;
							}
						}						
						if(rcii.chemical__r.PO__c){
							if(rcii.Minimum_Number_of_POs__c == null){
								rciiFields.add('Minimum Number of POs');
								reviewCheck = false;
							}
							if(rcii.Number_of_POs__c == null){
								rciiFields.add('Maximum Number of POs');
								reviewCheck = false;
							}
						}		
			
					}

					if (rcii.Component_Class__c == 'Polymer'){
                		if(rcii.Average_MW__c == null){
                			rciiFields.add('Average MW');
							reviewCheck = false;
                		}
                		if(rcii.Number_of_repeating_units__c == null){
                			rciiFields.add('Number of repeating units');
							reviewCheck = false;
                		}
                		if(rcii.Percent_MW_500__c == null){
                			rciiFields.add('Percent MW < 500');
							reviewCheck = false;
                		}
                		if(rcii.Percent_MW_1_000__c == null){
                			rciiFields.add('Percent MW < 1,000');
							reviewCheck = false;
                		}
                		if(rcii.Monomer_Ratio__c == null){
                			rciiFields.add('Monomer Ratio');
							reviewCheck = false;
                		}
                		if(rcii.Is_this_a_block_co_polymer__c == null){
                			rciiFields.add('Is this a block co-polymer');
							reviewCheck = false;
                		}
                		if(rcii.Polymer_Soluble_Dispersable_Swellable__c == null){
                			rciiFields.add('Polymer Soluble/Dispersable/Swellable');
							reviewCheck = false;
                		}
                	
                	}

					if ((rcii.Component_Class__c == 'Enzyme' || rcii.Component_Class__c == 'Enzyme and Enzyme Stabilizer')){
	                	if(rcii.Source_ID__c == null){
                			rciiFields.add('Source ID');
							reviewCheck = false;
                		}
	                	if(rcii.Residual_Microorganism_Level__c == null){
                			rciiFields.add('Residual Microorganism Level');
							reviewCheck = false;
                		}
	                	if(rcii.Is_this_enzyme_encapsulated__c == null){
                			rciiFields.add('Is this enzyme encapsulated');
							reviewCheck = false;
                		}                		
                	}

                    if (rcii.Component_Ingredient__c != null && reviewCheck) ingredientIDs.add(rcii.Component_Ingredient__c);
                    
                    if(rciiFields.size()>0){
         				rciiNamesMap.put(rcii.Name,rciiFields);
          			}
          			
                }
				
				for(string rName : rciiNamesMap.keyset()){
                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Ingredient component <a href="/'+rciiNameID.get(rName)+'" target="_blank">'+rName+'</a> is missing required field(s) '+rciiNamesMap.get(rname)+'.'));                	
                }
             
				list<string> ingNames = new list<string>();
                for (Ingredient__c ing : [select id, Name, (Select id from Ingredient_Formulations__r) from Ingredient__c where id in :ingredientIDs]){
	                if (ing.Ingredient_Formulations__r == null || ing.Ingredient_Formulations__r.size() == 0){
	                    reviewCheck = false;
						ingNames.add(ing.Name);
	                }
	            }
            	if(!reviewCheck && ingNames.size()>0){	
            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'All ingredients '+ingNames+' contained in the attached ingredient formulation must have an attached ingredient formulation.'));
            	}

                AggregateResult ar = [select sum(Min_Percent_Composition__c) min, sum(Max_Percent_Composition__c) max from Relationship_Chem_Ing_Ing__c where Alternate_Supplier__c = false and Ingredient_Formulation__c = :revRec.Ingredient_Formulation__c];
            
                if ((Decimal)ar.get('min') > 100 || (Decimal)ar.get('max') < 100){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'The range of total min percent composition to total max percent composition for all non-alternate ingredient components must contain 100.'));

                    reviewCheck = false;
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'An Ingredient Formulation is required to submit this Review Recommendation.'));
                reviewCheck = false;
            }
        }        
        return reviewRecReady && reviewCheck;
    }
}