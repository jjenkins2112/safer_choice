@isTest
private class TPPRRBatchSubmissionController_Test {

    @TestSetup
    private static void testData(){
        TPP_RR_Batch__c batch = new TPP_RR_Batch__c();
        insert batch;
        Account acct = new Account(Name = 'Test Account');
        insert acct;
        
        Ingredient__c ing = new Ingredient__c(Name = 'Test Ing');
        insert ing;
        
        Formulation__c form = new Formulation__c(Name = 'Test Form');
        insert form;
        
        Ingredient_Formulation__c ingF = new Ingredient_Formulation__c(Ingredient__c = ing.Id);
        insert ingF;
        
        TPP_Review_Recomendation__c tpprr = new TPP_Review_Recomendation__c(TPP_RR_Batch__c = batch.ID, 
                                                                            Formulation__c = form.Id,
                                                                            RecordTypeID = [select id from RecordType where sobjectType = 'TPP_Review_Recomendation__c' and DeveloperNAme = 'Initial_Review_Full_Product'].ID,
                                                                            Overall_Recommendation__c = 'Test',
                                                                            Overall_Recommendation_Notes__c = 'Test',
                                                                            pH__c = 'Test',
                                                                            pH_Notes__c = 'Test',
                                                                            VOCs__c = 'Test',
                                                                            VOCs_Notes__c = 'Test',
                                                                            Flammability__c = 'Test',
                                                                            Flammability_Notes__c = 'Test',
                                                                            Performance__c = 'Test',
                                                                            Performance_Notes__c = 'Test',
                                                                            Primary_Packaging__c = 'Test',
                                                                            Primary_Packaging_Notes__c = 'Test',
                                                                            Ingredient_Disclosure__c = 'Test',
                                                                            Ingredient_Disclosure_Method__c = 'Test',
                                                                            Ingredient_Disclosure_Notes__c = 'Test',
                                                                            Total_of_target_for_improvement_ingredie__c = 'Test',
                                                                            Target_for_improvement_Ingredients_Notes__c = 'Test',
                                                                            Ingredients_of_Concern__c = 'Test',
                                                                            Ingredients_of_Concern_Notes__c = 'Test',
                                                                            Logo__c = 'Test',
                                                                            Logo_Notes__c = 'Test',
                                                                            Environmental_Marketing_Claims__c = 'Test',
                                                                            Environmental_Marketing_Claims_Notes__c = 'Test',
                                                                            Endorsement_Disclaimer__c = 'Test',
                                                                            Endorsement_Disclaimer_Notes__c = 'Test'
                                                                            );
        TPP_Review_Recomendation__c tpprr2 = new TPP_Review_Recomendation__c(TPP_RR_Batch__c = batch.ID, 
                                                                            RecordTypeID = [select id from RecordType where sobjectType = 'TPP_Review_Recomendation__c' and DeveloperNAme = 'Renewal'].ID,
                                                                            Overall_Recommendation__c = 'Test',
                                                                            Overall_Recommendation_Notes__c = 'Test',
                                                                            pH__c = 'Test',
                                                                            pH_Notes__c = 'Test',
                                                                            VOCs__c = 'Test',
                                                                            VOCs_Notes__c = 'Test',
                                                                            Flammability__c = 'Test',
                                                                            Flammability_Notes__c = 'Test',
                                                                            Performance__c = 'Test',
                                                                            Performance_Notes__c = 'Test',
                                                                            Primary_Packaging__c = 'Test',
                                                                            Primary_Packaging_Notes__c = 'Test',
                                                                            Ingredient_Disclosure__c = 'Test',
                                                                            Ingredient_Disclosure_Method__c = 'Test',
                                                                            Ingredient_Disclosure_Notes__c = 'Test',
                                                                            Total_of_target_for_improvement_ingredie__c = 'Test',
                                                                            Target_for_improvement_Ingredients_Notes__c = 'Test',
                                                                            Ingredients_of_Concern__c = 'Test',
                                                                            Ingredients_of_Concern_Notes__c = 'Test',
                                                                            Logo__c = 'Test',
                                                                            Logo_Notes__c = 'Test',
                                                                            Environmental_Marketing_Claims__c = 'Test',
                                                                            Environmental_Marketing_Claims_Notes__c = 'Test',
                                                                            Endorsement_Disclaimer__c = 'Test',
                                                                            Endorsement_Disclaimer_Notes__c = 'Test',
                                                                            Alts_to_target_for_improvement__c = 'Test',
                                                                            Alts_to_target_for_improvement_Notes__c = 'Test',
                                                                            Alts_to_Green_Chem_Challenge__c = 'Test',
                                                                            Alts_to_Green_Chem_Challenge_Notes__c = 'Test'
                                                                            );
        TPP_Review_Recomendation__c tpprr3 = new TPP_Review_Recomendation__c(TPP_RR_Batch__c = batch.ID, 
                                                                            RecordTypeID = [select id from RecordType where sobjectType = 'TPP_Review_Recomendation__c' and DeveloperNAme = 'Initial_Review_Formulation'].ID,
                                                                            Overall_Recommendation__c = 'Test',
                                                                            Overall_Recommendation_Notes__c = 'Test',
                                                                            pH__c = 'Test',
                                                                            pH_Notes__c = 'Test',
                                                                            VOCs__c = 'Test',
                                                                            VOCs_Notes__c = 'Test',
                                                                            Flammability__c = 'Test',
                                                                            Flammability_Notes__c = 'Test',
                                                                            Performance__c = 'Test',
                                                                            Performance_Notes__c = 'Test',
                                                                            Primary_Packaging__c = 'Test',
                                                                            Primary_Packaging_Notes__c = 'Test',
                                                                            Ingredient_Disclosure__c = 'Test',
                                                                            Ingredient_Disclosure_Method__c = 'Test',
                                                                            Ingredient_Disclosure_Notes__c = 'Test',
                                                                            Total_of_target_for_improvement_ingredie__c = 'Test',
                                                                            Target_for_improvement_Ingredients_Notes__c = 'Test',
                                                                            Ingredients_of_Concern__c = 'Test',
                                                                            Ingredients_of_Concern_Notes__c = 'Test',
                                                                            Logo__c = 'Test',
                                                                            Logo_Notes__c = 'Test',
                                                                            Environmental_Marketing_Claims__c = 'Test',
                                                                            Environmental_Marketing_Claims_Notes__c = 'Test',
                                                                            Endorsement_Disclaimer__c = 'Test',
                                                                            Endorsement_Disclaimer_Notes__c = 'Test'
                                                                            );
        TPP_Review_Recomendation__c tpprr4 = new TPP_Review_Recomendation__c(TPP_RR_Batch__c = batch.ID, 
                                                                            RecordTypeID = [select id from RecordType where sobjectType = 'TPP_Review_Recomendation__c' and DeveloperNAme = 'Reformulation'].ID,
                                                                            Overall_Recommendation__c = 'Test',
                                                                            Overall_Recommendation_Notes__c = 'Test',
                                                                            pH__c = 'Test',
                                                                            pH_Notes__c = 'Test',
                                                                            VOCs__c = 'Test',
                                                                            VOCs_Notes__c = 'Test',
                                                                            Flammability__c = 'Test',
                                                                            Flammability_Notes__c = 'Test',
                                                                            Performance__c = 'Test',
                                                                            Performance_Notes__c = 'Test',
                                                                            Primary_Packaging__c = 'Test',
                                                                            Primary_Packaging_Notes__c = 'Test',
                                                                            Ingredient_Disclosure__c = 'Test',
                                                                            Ingredient_Disclosure_Method__c = 'Test',
                                                                            Ingredient_Disclosure_Notes__c = 'Test',
                                                                            Total_of_target_for_improvement_ingredie__c = 'Test',
                                                                            Target_for_improvement_Ingredients_Notes__c = 'Test',
                                                                            Ingredients_of_Concern__c = 'Test',
                                                                            Ingredients_of_Concern_Notes__c = 'Test',
                                                                            Logo__c = 'Test',
                                                                            Logo_Notes__c = 'Test',
                                                                            Environmental_Marketing_Claims__c = 'Test',
                                                                            Environmental_Marketing_Claims_Notes__c = 'Test',
                                                                            Endorsement_Disclaimer__c = 'Test',
                                                                            Endorsement_Disclaimer_Notes__c = 'Test'
                                                                            );
        TPP_Review_Recomendation__c tpprr5 = new TPP_Review_Recomendation__c(TPP_RR_Batch__c = batch.ID, 
                                                                            RecordTypeID = [select id from RecordType where sobjectType = 'TPP_Review_Recomendation__c' and DeveloperNAme = 'CleanGredients'].ID,
                                                                            Overall_Recommendation__c = 'Test5',
                                                                            Overall_Recommendation_Notes__c = 'Test',
                                                                            Ingredient__c = ing.ID,
                                                                            Ingredient_Formulation__c = ingF.Id, 
                                                                            Data_on_New_Chemicals_Provided__c = 'Test', 
                                                                            Data_on_New_Chemicals_Provided_Notes__c = 'Test', 
                                                                            Yellow_Triangle_Ingredients_10__c = 'Test', 
                                                                            Yellow_Triangle_Ingredients_10_Notes__c = 'Test', 
                                                                            Residuals_of_Concern_0_01__c = 'Test', 
                                                                            Residuals_of_Concern_0_01_Notes__c = 'Test', 
                                                                            Acceptable_for_Direct_Release__c = 'Test', 
                                                                            Acceptable_for_Direct_Release_Notes__c = 'Test'
                                                                            );
        
        insert new List<TPP_Review_Recomendation__c>{tpprr, tpprr2, tpprr3, tpprr4, tpprr5};
        
        Attachment a = new Attachment(NAme = 'Test', body = blob.valueOf('Test'), parentId = tpprr.Id);
        insert a;
        
        Relationship_Chem_Ing_Formulation__c rcif = new Relationship_Chem_Ing_Formulation__c(Formulation__c = form.Id, Ingredient__c = ing.Id);
        insert rcif;
        
        Relationship_Chem_Ing_Ing__c rcii = new Relationship_Chem_Ing_Ing__c(Ingredient_Formulation__c = ingF.Id, Component_Ingredient__c = ing.Id);                                            
        insert rcii;
        
    }

	private static testMethod void test() {
        TPPRRBatchSubmissionController controller = new TPPRRBatchSubmissionController(new apexpages.standardController([select id from TPP_RR_Batch__c limit 1]));
        controller.getTPPRRs();
        controller.addNewTPPRR();
        controller.addExistingTPPRR();
        
	}
	
    @isTest
    private static void test2(){
        TPPRRBatchSubmissionController controller = new TPPRRBatchSubmissionController(new apexpages.standardController([select id from TPP_RR_Batch__c limit 1]));

        controller.sm = new Submission_Metric__c(TPP_Review_Recommendation__c = [select id from TPP_Review_Recomendation__c limit 1].ID);
        controller.saveTPPRR();
        controller.submitAll();
    }
    
    
   
    @isTest
    private static void test5(){
        TPPRRBatchSubmissionController controller = new TPPRRBatchSubmissionController(new apexpages.standardController([select id from TPP_RR_Batch__c limit 1]));
       
        chemical__c c1 = new Chemical__c(Name = 'Chem 1', class__c = 'Surfactant', EO__c = true, Direct_Release_Status__c = 'Not acceptable for direct release');
        chemical__c c2 = new Chemical__c(Name = 'Chem 2', class__c = 'Surfactant', PO__c = true, Direct_Release_Status__c = 'Not acceptable for direct release');
        insert new list<chemical__c>{c1, c2};
       
        //test functions
        test.startTest();
        
        //test rcii EO surfactant
        Relationship_Chem_Ing_Ing__c rcii = [select id from  Relationship_Chem_Ing_Ing__c limit 1];
        rcii.TPP_Status_Comments__c = 'test';
        rcii.chemical__c = c1.id;
        update rcii;
        
        controller.getTPPRRs();
        
        //test rcii PO surfactant
        rcii.chemical__c = c2.id;
        update rcii;
        
        controller.getTPPRRs();
        
        test.stopTest();
    }
    
    @isTest
    private static void test6(){
        TPPRRBatchSubmissionController controller = new TPPRRBatchSubmissionController(new apexpages.standardController([select id from TPP_RR_Batch__c limit 1]));
        
        TPP_Review_Recomendation__c tppReview = [SELECT Id FROM TPP_Review_Recomendation__c where Overall_Recommendation__c = 'Test5' limit 1];
        
        Ingredient__c ig1 = new Ingredient__c(Name = 'Ingredient 1', class__c = 'Polymer', Direct_Release_Status__c = 'Not acceptable for direct release');
        Ingredient__c ig2 = new Ingredient__c(Name = 'Ingredient 2', class__c = 'Enzyme', Direct_Release_Status__c = 'Not acceptable for direct release');
		insert new list<ingredient__c> {ig1, ig2};
		
		Ingredient_Formulation__c ingF1 = new Ingredient_Formulation__c (Ingredient__c = ig1.ID, class__c ='Polymer');
		Ingredient_Formulation__c ingF2 = new Ingredient_Formulation__c (Ingredient__c = ig2.ID, class__c ='Enzyme');
		insert new list<Ingredient_Formulation__c> {ingF1, ingF2};

        //test functions
        test.startTest();
        Relationship_Chem_Ing_Ing__c rcii = [select id from  Relationship_Chem_Ing_Ing__c limit 1];
  
        //test rcii polymer
        rcii.TPP_Status_Comments__c = 'test';
        rcii.Component_Ingredient__c = ig1.id;
		rcii.Ingredient_Formulation__c = ingF1.id;
		update rcii;
		tppReview.Ingredient_Formulation__c = ingF1.id;
		tppReview.Ingredient__c = ig1.id;
		update tppReview;
        
        controller.getTPPRRs();
        
        //test rcii enzyme
        rcii.Polymer_Soluble_Dispersable_Swellable__c = 'test';
        rcii.Component_Ingredient__c = ig2.id;
		rcii.Ingredient_Formulation__c = ingF2.id;
		update rcii;
        tppReview.Ingredient_Formulation__c = ingF2.id;
		tppReview.Ingredient__c = ig2.id;
		update tppReview;
		
        controller.getTPPRRs();
        
        test.stopTest();
    }
    
}