@isTest
private class Test_ToxDataDetail {

	@isTest
	private static void testName() {
		Chemical__c chem = new Chemical__c(name = '12345');
		insert chem;

		TPP_Chemical_Data__c tcd = new TPP_Chemical_Data__c(Chemical__c = chem.Id);
		insert tcd;

		ToxDataDetail controller = new ToxDataDetail(new ApexPages.standardController(tcd));
		controller.editEndpoint();
		controller.saveEdit();
		controller.aquaticType = 'Fish';
		controller.addNewAquatic();
		controller.aquaticType = 'Algae';
		controller.addNewAquatic();
		controller.aquaticType = 'Daphnia';
		controller.addNewAquatic();
		controller.cancelEndpoints();
		controller.humanType = 'Acute Mammalian Toxicity';
		controller.addNewHuman();
		controller.humanType = 'Repeated Dose';
		controller.addNewHuman();
		controller.humanType = 'Reproductive';
		controller.addNewHuman();
		controller.humanType = 'Developmental';
		controller.addNewHuman();
		controller.humanType = 'Neurotoxicity';
		controller.addNewHuman();
		
	}
	
	@isTest
	private static void testSaveMethods() {
		Chemical__c chem = new Chemical__c(name = '12345');
		insert chem;

		TPP_Chemical_Data__c tcd = new TPP_Chemical_Data__c(Chemical__c = chem.Id);
		insert tcd;

		ToxDataDetail controller = new ToxDataDetail(new ApexPages.standardController(tcd));
		
		controller.editEndpoint();
		controller.saveEdit();
		controller.humanType = 'Genetic Toxicity';
		controller.addNewHuman();
		controller.saveEndpoints();
		controller.aquaticType = 'Fish';
		controller.addNewAquatic();
		controller.saveEndpoints();
		controller.aquaticType = 'Algae';
		controller.addNewAquatic();
		controller.saveEndpoints();
		controller.aquaticType = 'Daphnia';
		controller.addNewAquatic();
		controller.saveEndpoints();
		
	}
	
	@isTest
	private static void testSaveMethods2() {
		Chemical__c chem = new Chemical__c(name = '12345');
		insert chem;

		TPP_Chemical_Data__c tcd = new TPP_Chemical_Data__c(Chemical__c = chem.Id);
		insert tcd;

		ToxDataDetail controller = new ToxDataDetail(new ApexPages.standardController(tcd));
		
		controller.editEndpoint();
		controller.saveEdit();
		
		controller.humanType = 'Acute Mammalian Toxicity';
		controller.addNewAquatic();
		controller.saveEndpoints();
		controller.humanType = 'Repeated Dose';
		controller.addNewAquatic();
		controller.saveEndpoints();
		controller.humanType = 'Reproductive';
		controller.addNewAquatic();
		controller.saveEndpoints();
		controller.humanType = 'Developmental';
		controller.addNewAquatic();
		controller.saveEndpoints();

	}
	
	@isTest
	private static void testButtonMethods() {
		Chemical__c chem = new Chemical__c(name = '12345');
		insert chem;

		TPP_Chemical_Data__c tcd = new TPP_Chemical_Data__c(Chemical__c = chem.Id);
		insert tcd;

		ToxDataDetail controller = new ToxDataDetail(new ApexPages.standardController(tcd));
		
		controller.editEndpoint();
		controller.saveEdit();
		controller.humanType = 'Neurotoxicity';
		controller.addNewHuman();
		controller.saveEndpoints();
		controller.humanType = 'Genetic Toxicity';
		controller.addNewHuman();
		controller.saveEndpoints();
		controller.deleteEndpoint();
		controller.cloneRecord();
		
		User u = [select id from User where isActive = true and Profile.Name = 'System Administrator' limit 1];
		system.runas(u){
			controller.claimRecord();
		}
		
	}
	
	@isTest
	private static void testButtonMethods2() {
		Chemical__c chem = new Chemical__c(name = '12345');
		insert chem;

		TPP_Chemical_Data__c tcd = new TPP_Chemical_Data__c(Chemical__c = chem.Id);
		insert tcd;

		User u = [select id from User where isActive = true and Profile.Name like 'Third Party Profile%' limit 1];
		TPP_Chemical_Data__Share tppShare = new TPP_Chemical_Data__Share();
		tppShare.ParentId = tcd.id;
		tppShare.UserOrGroupId = u.id;
		tppShare.ACCESSLEVEL = 'Read';
//		tppShare.ROWCAUSE = 'Rule';
		insert tppShare;
		
		system.runas(u){
			ToxDataDetail controller = new ToxDataDetail(new ApexPages.standardController(tcd));
			controller.claimRecord();
		}

	}
}