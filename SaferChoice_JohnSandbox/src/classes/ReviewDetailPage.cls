public class ReviewDetailPage {     
    @TestVisible private Review__c rev {get;set;}
    @TestVisible private List<ChemicalWrapper> chemWrappers;
    private Map<String, RecordType> rtMap;
    private id formID;
    @TestVisible private id prodID;
    private id ingID;
    private id ingForID;
    private final string NOTIFICATIONEMAILSUBJECT = 'Second Review has been completed';
    private final string NOTIFICATIONEMAILBODY = 'Please note that the second review for tppRevRecNum has been completed by secondReviewer. <br/><br/> Company Name: companyName <br/> Product/Ingredient Name: prodIngName <br/> <br/>Please review in Salesforce at: reviewLink';
    private final string FIELDSREQUIREDERROR = 'All fields must be filled into complete review.';
    private final string WORKGROUPREQERROR = 'Each selected Chemical or Ingredient needs to have a Workgroup assigned';
    private string url;
    private ApexPages.StandardController stdController;
    private final static string NEWSTATUS = 'New';
    private final static string FIRSTCLAIMSTATUS = 'First Review Claimed';
    private final static string SECONDCLAIMSTATUS = 'Second Review Claimed';
    private final static string SECONDCLAIMSTATUSRESUB = 'Resubmitted, Second Review Claimed';
    private final static string FIRSTCOMPLETESTATUS = 'First Review Completed';
    private final static string SECONDCOMPLETESTATUS = 'Second Review Completed';
    @TestVisible private final static string FINALCOMPLETESTATUS = 'Complete and Feedback Email Sent';
    @TestVisible private final static string ADDEDRENEWEDSTATUS = 'Product Added/Renewed';
    @TestVisible private final static string COMPLETEAPPROVEDSTATUS = 'Complete and Approved';
    @TestVisible private final static string NOTMOVINGFORWARDSTATUS = 'Not Moving Forward';

    public boolean updateFirstReview {get;set;}
    public boolean updateSecondReview {get;set;}
    public id tppReviewRecID {get;set;}
    public string draftAutomatedEmail {get;set;}
    public boolean isReviewer {get;set;}
    public TPP_Review_Recomendation__c tppRevRec {get;set;}
    public boolean revRecLocked {get;set;}
    public string additionalProductNames {get;set;}
    public boolean isAdmin {get;set;}
    @TestVisible private List<Label> labels;

    public ReviewDetailPage (ApexPages.StandardController stdController){
        //url = ApexPages.currentPage().getUrl();
        
        //stdController.addFields(new List<String>{'Formulation__c'});
        this.rev = (Review__c) stdController.getRecord();
        url = 'https://' +ApexPages.currentPage().getHeaders().get('host') + '/apex/ReviewDetailPage?id=' + rev.ID;

        resetTPPRevRec();

        additionalProductNames = '';
        for (Relationship_Recommendation_Product__c addlProd : [select id, Product__r.Name from Relationship_Recommendation_Product__c where TPP_Review_Recommendation__c = :tppRevRec.ID]){
            additionalProductNames += addlProd.Product__r.Name + ' - ';
        }

        additionalProductNames = additionalProductNames.removeEnd(' - ');
        system.debug(tppREvRec);
        formID = tppRevRec.Formulation__c;
        prodId = tppRevRec.Product__c;
        ingID = tppRevRec.Ingredient__c;
        ingForID = tppRevRec.Ingredient_Formulation__c;
        tppReviewRecID = tppRevRec.ID;
        revRecLocked = tppRevRec.RecordType != null ? tppRevRec.RecordType.Name.contains('Locked') : false;
        draftAutomatedEmail = '/apex/automatedEmails_menu?sentID=' + tppReviewRecID;

        rtMap = UtilityMethods.returnRTMap('Agenda_Item__c');

        this.stdController = stdController;

        updateFirstReview = false;
        updateSecondReview = false;

        Map<id, boolean> reviewers = UtilityMethods.usersInGroup(new Set<id>{UserInfo.getUserID()}, [select id from Group where DeveloperName = 'Safer_Choice_Reviewers'].ID);
        isreviewer = reviewers.get(UserInfo.getUserID());

        if (userInfo.getProfileID() == [select id from Profile where Name = 'System Administrator'].ID) isAdmin = true;
        else isAdmin = false;
    }

    public PageReference filter(){
        return null;
    }

    //assigns review to self
    public PageReference assignToSelf(){
        if (rev.Reviewer_1__c == null){
            rev.Reviewer_1__c = UserInfo.getUserID();
            rev.Status__c = FIRSTCLAIMSTATUS;

            if (tppRevRec.Product__c != null){
                updateProd(tppRevRec, UtilityMethods.cleanRT(tppRevRec.RecordType.DeveloperName) + ' under review with Safer Choice');
            }
        }
        else if (rev.Reviewer_2__c == null && rev.Reviewer_1__c != UserInfo.getUserID()){
            rev.Reviewer_2__c = UserInfo.getUserID();
            if (!rev.Status__c.contains('Resubmitted')) rev.Status__c = SECONDCLAIMSTATUS;
            else rev.Status__c = SECONDCLAIMSTATUSRESUB;
        }
        //DML-120
        update rev;

        return null;
    }

    //mark feedback email sent
    public PageReference sendEmail(){
        rev.Feedback_Email_Sent__c = true;
        rev.Status__c = FINALCOMPLETESTATUS;
        rev.Feedback_Email_Sent_Date__c = date.today();
        //DML-121
        update rev;

        if (tppRevRec.Product__c != null){
            updateProd(tppRevRec, 'Safer Choice sent feedback for ' + UtilityMethods.cleanRT(tppRevRec.RecordType.DeveloperName) + ', waiting for partner response');
        }

        updateRevRecStatus();

        return null;
    }

    //update add/newed checkbox
    public PageReference addedRenewed(){
        rev.Product_Added_Renewed__c = true;
        rev.Status__c = ADDEDRENEWEDSTATUS;
        rev.Product_Added_Date__c = date.today();
        //DML-121
        update rev;

        if (tppRevRec.Product__c != null){
            String updateTerm = ' added/renewed';
            if (tppRevRec.RecordType.DeveloperName.contains('Reformulation')) updateTerm = ' updated (reformulation)';
            else if (tppRevRec.RecordType.DeveloperName.contains('Renewal')) updateTerm = ' renewed';
            else if (tppRevREc.RecordType.DeveloperName.contains('Inital_Review')) updateTerm = ' added';
            updateProd(tppRevRec, 'Product' + updateTerm);
        }
        else{
            tppRevRec.Safer_Choice_Review_Status__c = 'Ingredient Added/Renewed';
        }

        updateRevRecStatus();

        return null;
    }

    //mark completed and approved
    public PageReference completeAndApproved(){
        rev.Complete_and_Approved__c = true;
        rev.Status__c = COMPLETEAPPROVEDSTATUS;
        rev.Completed_and_Approved_Date__c = date.today();
        //DML-122
        update rev;

        if (tppRevRec.Product__c != null){
            updateProd(tppRevRec, 'Product ' + UtilityMethods.cleanRT(tppRevRec.RecordType.DeveloperName) + ' approved, agreement/amendment in progress');
        }

        updateRevRecStatus();

        return null;
    }

    public PageReference notMovingForward(){
        rev.Not_Moving_Forward__c = true;
        rev.Status__c = NOTMOVINGFORWARDSTATUS;
        rev.Not_Moving_Forward_Date__c = date.today();
        //DML-123
        update rev;

        updateRevRecStatus();

        return null;
    }

    public PageReference saveInline(){
        if (rev.Feedback_Email_Sent__c){
            rev.Status__c = FINALCOMPLETESTATUS;
            if (rev.Feedback_Email_Sent_Date__c == null) rev.Feedback_Email_Sent_Date__c = date.today();
        } 
        else rev.Feedback_Email_Sent_Date__c = null;

        if (rev.Complete_and_Approved__c){
            rev.Status__c = COMPLETEAPPROVEDSTATUS;
            if (rev.Completed_and_Approved_Date__c == null) rev.Completed_and_Approved_Date__c = date.today();
        }
        else rev.Completed_and_Approved_Date__c = null;

        
        if (rev.Product_Added_Renewed__c){
            rev.Status__c = ADDEDRENEWEDSTATUS;
            if (rev.Product_Added_Date__c == null) rev.Product_Added_Date__c = date.today();
        }
        else rev.Product_Added_Date__c = null;
        
        if (rev.Not_Moving_Forward__c){
            rev.Status__c = NOTMOVINGFORWARDSTATUS;
            if (rev.Not_Moving_Forward_Date__c == null) rev.Not_Moving_Forward_Date__c = date.today();
        }
        else rev.Not_Moving_Forward_Date__c = null;
        //DML-124
        update rev;

        updateRevRecStatus();

        return null;
    }

    private void updateRevRecStatus(){
        if (tppRevRec.Product__c != null){
            Product__c prod2 = [select id, 
                                        Name, 
                                        Waiting_For_Supplier__c, 
                                        Supplier_Info_Collected__c, 
                                        Third_Party_Review_Completed__c, 
                                        Not_Moving_forward__c,
                                        Control_Code__c
                                        from Product__c 
                                        where id = :tppRevRec.Product__c];
            ProductTrackingStatus.prod p = new ProductTrackingStatus.prod(prod2.Name, prod2.ID, prod2.Control_Code__c);

            p = ProductTrackingStatus.setProdStatus(prod2, p, PRoductTrackingStatus.prodToTPPRevRec(new set<id>{prod2.ID}), false);

            tppRevRec.Safer_Choice_Review_Status__c = p.scReviewStatus;
            tppRevRec.TPP_Review_Status__c = p.tppReviewStatus;
        
            //DML-125
            update tppRevRec;
        }
        else{
            //Set Safer Choice status based on checkboxes on review
			if (rev.Feedback_Email_Sent__c) tppRevRec.Safer_Choice_Review_Status__c = 'Safer Choice review complete, see feedback email for outstanding issues';
			if (rev.Complete_and_Approved__c) tppRevRec.Safer_Choice_Review_Status__c = 'Comple and approved';
			if (rev.Product_Added_Renewed__c) tppRevRec.Safer_Choice_Review_Status__c = 'Ingredient added/renewed';
			if (rev.Not_Moving_Forward__c) tppRevRec.Safer_Choice_Review_Status__c = 'Not moving forward';
			
			update tppRevRec;
        }
    }


    //send notification email to first notifier
    public PageReference notifyFirstReviewer(){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[]{};
        toAddresses.add([select Email from User where id = :rev.Reviewer_1__c].Email);
        mail.setToAddresses(toAddresses);

        mail.setSubject(NOTIFICATIONEMAILSUBJECT);
        String body = NOTIFICATIONEMAILBODY.replace('secondReviewer', [select Name from User where id =:rev.Reviewer_2__c].Name);
        body = body.replace('reviewLink', '<a href="' + url + '">' + url + '</a>');
        body = body.replace('tppRevRecNum', tppRevRec.Name);
        body = body.replace('companyName', tppRevRec.Product_Company__c != null ? tppRevrec.Product_Company__c : '');
        if (tppRevRec.Product__c != null && tppRevRec.Product__r.Name != null){
            body = body.replace('prodIngName', tppRevRec.Product__r.Name);
        }
        else if (tppRevRec.Ingredient__c != null && tppRevRec.Ingredient__r.Name != null){
            body = body.replace('prodIngName', tppRevRec.Ingredient__r.Name);
        }
        else{
            body = body.replace('prodIngName', '');
        }
        mail.setHTMLBody(body);

        if (!test.isRunningTest()) Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        return null;
    }

    //open edit page
    public PageReference updateReview(){
        if (rev.Reviewer_1__c == UserInfo.getUserID()){
            updateFirstReview = true;
            updateSecondReview = false;
        }
        else if (rev.Reviewer_2__c == UserInfo.getUserID()){
            updateSecondReview = true;
            updateFirstReview = false;

            if (rev.Reviewer_2_Notes__c == null) rev.Reviewer_2_Notes__c = rev.Reviewer_1_Notes__c;
        }
        return null;
    }

    //saves Review changes
    public PageReference save(){
        checkBlankReviewer();
        //DML-126
        update rev;

        updateFirstReview = false;
        updateSecondReview = false;

        return null;
    }

    //checks for required fields, then saves if able
    public PageReference saveAndComplete(){
        if (updateFirstReview){
            if (rev.Reviewer_1__c == null || rev.Review_1_Date__c == null || rev.Reviewer_1_Notes__c == null || rev.Recommendation_1__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, FIELDSREQUIREDERROR));

                return null;
            }

            rev.Status__c = FIRSTCOMPLETESTATUS;
        } 
        else if (updateSecondReview){
            if (rev.Reviewer_2__c == null || rev.Review_2_Date__c == null || rev.Reviewer_2_Notes__c == null || rev.Recommendation_2__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, FIELDSREQUIREDERROR));

                return null;
            }

            rev.Status__c = SECONDCOMPLETESTATUS;
            notifyFirstReviewer();
        } 
        
        save();

        return null;
    }

    //checks if reviewer is no longer assigned
    @TestVisible
    private void checkBlankReviewer(){
        if (updateFirstReview && rev.Reviewer_1__c == null) {
            rev.Status__c = NEWSTATUS;
        }
        else if (updateSecondReview && rev.Reviewer_2__c == null){
            rev.Status__c = FIRSTCOMPLETESTATUS;
        } 
    }

    //finds chemicals related to TPP Rev Rec
    public List<ChemicalWrapper> getChemicals(){
        if (chemWrappers == null){
            chemWrappers = new List<ChemicalWrapper>();

            Map<id,id> chemIDs = new Map<id,id>();
            Map<id,id> ingIds = new Map<id,id>();
            if(ingForID == null){
                List<Relationship_Chem_Ing_Formulation__c> rels = [select id, Chemical__c, Ingredient__c from Relationship_Chem_Ing_Formulation__c where Formulation__c = :formID];
                for (Relationship_Chem_Ing_Formulation__c rel : rels){
                    chemIds.put(rel.Chemical__c, rel.ID);
                    ingIds.put(rel.Ingredient__c, rel.ID);
                }
                
                for (Chemical__c chem : [select id, Name, Tradename__c, Class__c from Chemical__c where id in :chemIDs.keyset()]){
                    chemWrappers.add(new ChemicalWrapper(chem, false, new Agenda_Item__c(Chemical__c = chem.ID, Formulation_Component__c = chemIds.get(chem.ID))));
                }
    
                for (Ingredient__c ing : [select id, Name, Tradename__c, Class__c from Ingredient__c where id in :ingIDs.keyset()]){
                    chemWrappers.add(new ChemicalWrapper(ing, false, new Agenda_Item__c(Ingredient__c = ing.ID, Formulation_Component__c = ingIds.get(ing.ID))));
                }
            }
            else{
                for (Relationship_Chem_Ing_Ing__c rel : [select id, Chemical__c, Component_Ingredient__c from Relationship_Chem_Ing_Ing__c where Ingredient_Formulation__c = :ingForID]){
                    chemIds.put(rel.Chemical__c, rel.ID);
                    ingIds.put(rel.Component_Ingredient__c, rel.ID);
                }
    
                for (Chemical__c chem : [select id, Name, Tradename__c, Class__c from Chemical__c where id in :chemIDs.keyset()]){
                    chemWrappers.add(new ChemicalWrapper(chem, false, new Agenda_Item__c(Chemical__c = chem.ID, Ingredient_Component__c = chemIds.get(chem.ID))));
                }
    
                for (Ingredient__c ing : [select id, Name, Tradename__c, Class__c from Ingredient__c where id in :ingIDs.keyset()]){
                    chemWrappers.add(new ChemicalWrapper(ing, false, new Agenda_Item__c(Ingredient__c = ing.ID, Ingredient_Component__c = ingIds.get(ing.ID))));
                }
            }

            
        }

        return chemWrappers;
    }

    //process selected chemicals and adds as work items to workgroups
    public PageReference submitToWorkgroup(){
        List<Agenda_Item__c> agendaItems = new List<Agenda_Item__c>();
        List<String> rtIDs = new List<string>();
        for(ChemicalWrapper cw : chemwrappers){
            if (cw.submit && cw.agItem.Workgroup__C == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.error, WORKGROUPREQERROR));

                return null;
            }
        }

        for(ChemicalWrapper cw : chemwrappers){
            if (cw.submit && cw.agItem.Workgroup__C != null){
                agendaItems.add(cw.agItem);
                rtIDs.add(rtMap.get(cw.cwType).ID);
            } 
        }

        processSubmissions(agendaItems, rtIds);

        return null;
    }
    
    public pageReference lockFormulation(){
        UtilityMethods.lockFormulations(new set<id>{tppRevRec.Formulation__c}, true);
        
        return null;
    }
    
    public PageReference unlockFormulation(){
        UtilityMethods.lockFormulations(new set<id>{tppRevRec.Formulation__c}, false);
        
        Attachment a = new Attachment();
        a.Name = 'Locked Formulation.xls';
        PageReference pageRef = Page.cloneFormulation_export_excel;
        pageRef.getParameters().put('sentId', formID);
        if (!Test.isRunningTest()) a.Body = pageRef.getContent();
        else a.Body = blob.valueOf('Test');
        a.ParentID = tppReviewRecID;
        //DML-127
        insert a;
        
        return null;
    }
    
    public PageReference lockProduct(){
        UtilityMethods.lockProducts(new set<id>{tppRevRec.Product__c}, true);
        
        resetTPPRevRec();
        return null;
    }
    
    public PageReference unlockProduct(){
        UtilityMethods.lockProducts(new set<id>{tppRevRec.Product__c}, false);
        
        resetTPPRevRec();
        return null;
    }
    
    public PageReference lockIngredient(){
        UtilityMethods.lockIngredients(new set<id>{tppRevRec.Ingredient__c}, true);
        
        resetTPPRevRec();

        return null;
    }
    
    public PageReference unlockIngredient(){
        UtilityMethods.lockIngredients(new set<id>{tppRevRec.Ingredient__c}, false);
        
        resetTPPRevRec();
        return null;
    }
    
    public PageReference lockTppRevRec(){
        Map<id, Id> rtIDMap = UtilityMethods.lockTPPReviewRecs(new set<id>{tppRevRec.ID});
        tppRevRec.RecordTypeID = rtIDMap.get(tppRevRec.ID);
        //DML-128
        update tppRevRec;
        revRecLocked = true;
        
        resetTPPRevRec();
        return null;
    }
    
    public PageReference unlockTppRevRec(){
         Map<id, Id> rtIDMap = UtilityMethods.unlockTPPReviewRecs(new set<id>{tppRevRec.ID});
        tppRevRec.RecordTypeID = rtIDMap.get(tppRevRec.ID);
        //DML-129
        update tppRevRec;
        revRecLocked = false;
        
        string tppPMEmail = tppRevRec.TPP_Project_Manager__c != null ? [select Email from User where Name = :tppRevRec.TPP_Project_Manager__c and Profile.Name like '%Third Party Profiler%' limit 1].Email : '';
        string tppReviewerEmail = tppRevRec.TPP_Reviewer__c != null ? [select email from User where Name = :tppRevRec.TPP_Reviewer__r.Name  and Profile.Name like '%Third Party Profiler%' limit 1].Email : '';
        List<String> addresses = new List<String>{tppPMEmail};
        //only add tppReviewer if separate from TPP PM
        if (tppReviewerEmail != '' && tppReviewerEmail != tppPMEmail) addresses.add(tppReviewerEmail);
        if (tppRevRec.Unlock_Requester_Email__c != null 
                && tppRevRec.Unlock_Requester_Email__c != ''
                && !addresses.contains(tppRevRec.Unlock_Requester_Email__c)) addresses.add(tppRevrec.Unlock_Requester_Email__c);
        
        system.debug(addresses);
        UtilityMethods.sendSimpleEmail(addresses, new List<String>(), tppRevRec.Name + ' has been unlocked', tppRevRec.Name + ' has been unlocked by ' + UserInfo.getName() + '.');
        resetTPPRevRec();

        return null;
    }

    @TestVisible
    private static void updateProd(TPP_Review_Recomendation__c tppRevRec, String reviewStatus){
        Product__c prod = new Product__c(id = tppRevRec.Product__c);
        prod.Product_Review_Status__c = reviewStatus;
        //DML-130
        update prod;
    }

    public void resetTPPRevRec(){
        try {
            tppRevRec = [select ID, 
                                Name,
                                Product_Company__c,
                                RecordType.Name, 
                                RecordType.DeveloperName,
                                Formulation__c, 
                                Formulation__r.RecordType.Name, 
                                Product__c, 
                                Product__r.Name,
                                Product__r.RecordType.Name, 
                                Ingredient__c, 
                                Ingredient_Formulation__c,
                                Ingredient__r.Name,
                                Ingredient__r.RecordType.Name,
                                TPP_Project_Manager__c,
                                TPP_Reviewer__c,
                                TPP_Reviewer__r.Name,
                                Unlock_Requester_Email__c
                                from TPP_Review_Recomendation__c 
                                where Review__c = :rev.ID limit 1];
        }
        catch (Exception e){
            tppRevRec = new TPP_Review_Recomendation__c();
        }
    }
    
    //submits chemicals to work group
    private static void processSubmissions(List<Agenda_Item__c> agItems, List<ID> rtIDs){
        Integer i = 0;
        for (Agenda_Item__c agItem : agItems){
            agItem.RecordTypeID = rtIDs[i];
            i += 1;
        }

        //DML-131
        insert agItems;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Agenda Items added!'));

    }
    
    public List<Label> getLabels(){
        labels = new list<Label>();
        
        for (Attachment a : [select id, name, LastModifiedDate from Attachment where parentId = :tppReviewRecID]){
            labels.add(new Label(a));
        }
        return labels;
    }
    
    public pageReference markLabels(){
        string finalApprovedLabelID = '';
        
        for (Label l: labels) if (l.selected) finalApprovedLabelID += l.attachID + ';';
        if (prodId != null){
            update new Product__c(id = prodId, Final_Approved_Label_IDs__c = finalApprovedLabelId);
        }
        
        return null;
    }

    public class ChemicalWrapper{
        public Chemical__c chem {get;set;}
        public Ingredient__c ing {get;set;}
        public boolean submit {get;set;}
        public Agenda_Item__c agItem {get;set;}
        public string cwType {get;set;}

        public ChemicalWrapper(Chemical__c chem, boolean submit, Agenda_Item__c agItem){
            this.chem = chem;

            this.submit = submit;
            this.agItem = agItem;
            this.cwType = 'Chemical';
        }

        public ChemicalWrapper(Ingredient__c ing, boolean submit, Agenda_Item__c agItem){
            this.ing = ing;
            this.submit = submit;
            this.agItem = agItem;
            this.cwType = 'Ingredient';
        }
    }
    
    public class Label{
        public boolean selected {get;set;}
        public string attachName {get;set;}
        public id attachId {get;set;}
        public string labelNote {get;set;}
        public string attachLastModified {get;set;}

        public Label(Attachment a){
            this.attachName = a.Name;
            this.attachID = a.ID;
            this.selected = false;
            if (a!= null && a.LastModifiedDate != null) this.attachLastModified = a.LastModifiedDate.format();
        }
        
        public Label(Attachment a, boolean selected){
            this.attachName = a.Name;
            this.attachID = a.ID;
            this.selected = selected;
            if (a!= null && a.LastModifiedDate != null) this.attachLastModified = a.LastModifiedDate.format();
        }
    }
}