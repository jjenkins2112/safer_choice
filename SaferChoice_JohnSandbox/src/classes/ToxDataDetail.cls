public class ToxDataDetail { 
	private TPP_Chemical_Data__c tcd;

	public string aquaticType {get;set;}
	public string humanType {get;set;}

	public boolean fishInputs {get;set;}
	public boolean daphniaInputs {get;set;}
	public boolean algaeInputs {get;set;}

	public boolean mammalInputs {get;set;}
	public boolean doseInputs {get;set;}
	public boolean reproductiveInputs {get;set;}
	public boolean developmentalInputs {get;set;}
	public boolean neurotoxicityInputs {get;set;}
	public boolean geneticToxicityInputs {get;set;}

	public id deleteTCDId {get;set;}
	public id cancelEPId {get;set;}
	public id editTCDID {get;set;}
	public id cuID {get;set;}
	public id cuPID {get;set;}
	public boolean isEditing {get;set;}
	public boolean isAdmin {get;set;}
	public boolean isTppOwner {get;set;}
	public boolean isTppUser {get;set;}
	public boolean readOnly {get;set;}

	public ToxDataDetail(ApexPages.standardController stdController){
		this.tcd = (TPP_Chemical_Data__c)stdcontroller.getRecord();
		
		fishInputs = false;
		daphniaInputs = false;
		algaeInputs = false;

		mammalInputs = false;
		doseInputs = false;
		reproductiveInputs = false;
		developmentalInputs = false;
		neurotoxicityInputs = false;
		geneticToxicityInputs = false;

		isEditing = false;
		
		cuID = UserInfo.getUserId();
		cuPID = UserInfo.getProfileId();
		
		readOnly = false;
		isTppOwner = false;
		isTppUser = false;
		if(userInfo.getProfileID() == [select id from Profile where Name = 'System Administrator'].ID) {
			isAdmin = true;
		}
        else {
        	isAdmin = false;
        	list<profile> ps = [select id from Profile where Name like 'Third Party Profiler%'];
        	
        	user owner;
        	if (!Test.isRunningTest()){owner = [select id, profileid from user where id = :tcd.ownerid limit 1];}
        	else {id oid = [select ownerid from TPP_Chemical_Data__c where id = :tcd.id limit 1].ownerid;
        		owner = [select id, profileid from user where id = :oid limit 1];
        	}
        	for(profile p : ps){
	    		if(owner.profileid == p.ID) {
    				isTppOwner = true;
	    		}	
	    		if(cuPID == p.ID){
	    			isTppUser = true;
	    		}
			}
    	}
    	
    	if(!isAdmin && isTPPOwner && !isTppUser){
    		readOnly = true;
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'This is a TPP-owned record. Please use the record owned by your group. Any changes will not be saved.'));
    	}

		resetEndpoints();
	}

	public pageReference saveEdit(){
		upsert tcd;
		upsert [select id from Endpoint__c where TPP_Chemical_Data__c = :tcd.ID];

		return new pageReference('/'+tcd.ID);
	}

	public pageReference editEndpoint(){
		return null;
	}

	public void resetEndpoints(){
		fishAquaticEps = returnAquaticEndpoints('Fish');
		daphniaAquaticEPs = returnAquaticEndpoints('Daphnia');
		algaeAquaticEPs = returnAquaticEndpoints('Algae');
	
		acuteMammalianEPs = returnEndpoints('Acute Mammalian Toxicity');
		repeatedDoseEPs = returnEndpoints('Repeated Dose');
		reproductiveEPs = returnEndpoints('Reproductive');
		developmentalEPs = returnEndpoints('Developmental');
		neurotoxicityEPs = returnEndpoints('Neurotoxicity');
		geneticToxicityEPs = returnEndpoints('Genetic Toxicity');

	}


	public pageReference deleteEndpoint(){
		delete [select id from Endpoint__c where id=:deleteTCDId];

		resetEndpoints();
		return null;
	}

	public pageReference cancelEndpoints(){
		delete [select id from Endpoint__c where id = :cancelEPID];

		fishInputs = false;
		daphniaInputs = false;
		algaeInputs = false;

		mammalInputs = false;
		doseInputs = false;
		reproductiveInputs = false;
		developmentalInputs = false;
		neurotoxicityInputs = false;
		geneticToxicityInputs = false;

		editTCDID = null;
		isEditing = false;

		resetEndpoints();
		return null;
	}

	public pageReference saveEndpoints(){
		if (aquaticType == 'Fish'){
			update fishAquaticEPs;
			fishInputs = false;
		}
		else if (aquaticType == 'Daphnia'){
			update daphniaAquaticEPs;
			daphniaInputs = false;
		}
		else if (aquaticType == 'Algae'){
			update algaeAquaticEPs;
			algaeInputs = false;
		}
		
		if (humanType == 'Acute Mammalian Toxicity'){
			update acuteMammalianEPs;
			mammalInputs = false;
		}
		else if (humanType == 'Repeated Dose'){
			update repeatedDoseEPs;
			doseInputs = false;
		}
		else if (humanType == 'Reproductive'){
			update reproductiveEPs;
			reproductiveInputs = false;
		}
		else if (humanType == 'Developmental'){
			update developmentalEPs;
			developmentalInputs = false;
		}
		else if (humanType == 'Neurotoxicity'){
			update neurotoxicityEPs;
			neurotoxicityInputs = false;
		}
		else if (humanType == 'Genetic Toxicity'){
			update geneticToxicityEPs;
			geneticToxicityinputs = false;
		}

		isEditing = false;
		editTCDID = null;
		resetEndpoints();

		return null;
	}

	public pageReference addNewAquatic(){
		updateEPs();

		Endpoint__c ep = new Endpoint__c(TPP_Chemical_Data__c = tcd.Id, Aquatic_Toxicity_Type__c	= aquaticType);
		insert ep;
		cancelEPID = ep.Id;

		if (aquaticType == 'Fish') fishInputs = true;
		else if (aquaticType == 'Daphnia') daphniaInputs = true;
		else if (aquaticType == 'Algae') algaeInputs = true;

		isEditing = true;

		resetEndpoints();

		return null;
	}

	public pageReference addNewHuman(){
		updateEPs();

		Endpoint__c ep = new Endpoint__c(TPP_Chemical_Data__c = tcd.ID, Endpoint__c = humanType);
		insert ep;
		cancelEPId = ep.ID;

		if (humanType == 'Acute Mammalian Toxicity') mammalInputs = true;
		else if (humanType == 'Repeated Dose') doseInputs = true;
		else if (humanType == 'Reproductive') reproductiveInputs = true;
		else if (humanType == 'Developmental') developmentalInputs = true;
		else if (humanType == 'Neurotoxicity') neurotoxicityInputs = true;
		else if (humanType == 'Genetic Toxicity') geneticToxicityInputs = true;

		isEditing = true;

		resetEndpoints();

		return null;
	}
	
	public pageReference cloneRecord(){

		TPP_Chemical_Data__c tppcd = database.query(UtilityMethods.buildQueryAllString('TPP_Chemical_Data__c','id = \''+tcd.id+'\''));
		system.debug(tppcd);
		TPP_Chemical_Data__c newTPPCD = tppcd.clone(false, true, false, false);
		insert newTPPCD;
		system.debug('newTPPCD.ID: '+newTPPCD.ID);
		List<Endpoint__c> endPoints = database.query(UtilityMethods.buildQueryAllString('Endpoint__c','TPP_Chemical_Data__c =\'' + tcd.ID +'\''));
		List<Endpoint__c> newEndPoints = new List<Endpoint__c>();
		for (Endpoint__c endpoint : endPoints){
		    Endpoint__c newEndPoint = endPoint.clone(false, true, false, false);
		    newEndpoint.TPP_Chemical_Data__c = newTPPCD.ID;
		    newEndpoints.add(newEndpoint);
		}
		insert newEndpoints;		
    
		return new pageReference('/'+newTPPCD.ID);

	}
	
	public pageReference claimRecord(){
		
    	try{
    		tcd.ownerid = cuID;
    		update tcd;
    	} catch(exception e){system.debug('exception! : '+e);}
		return null;
	}
	
	private void updateEPs(){
		update acuteMammalianEPs;
		update repeatedDoseEPs;
		update reproductiveEPs;
		update developmentalEPs;
		update neurotoxicityEPs;
		update geneticToxicityEPs;

		update fishAquaticEPs;
		update daphniaAquaticEPs;
		update algaeAquaticEPs;
	}

	public List<Endpoint__c> acuteMammalianEPs {get;set;}
	public List<Endpoint__c> repeatedDoseEPs {get;set;}
	public List<Endpoint__c> reproductiveEPs {get;set;}
	public List<Endpoint__c> developmentalEPs {get;set;}
	public List<Endpoint__c> neurotoxicityEPs {get;set;}
	public List<Endpoint__c> geneticToxicityEPs {get;set;}

	public List<Endpoint__c> fishAquaticEPs {get;set;}
	public List<Endpoint__c> daphniaAquaticEPs{get;set;}
	public List<Endpoint__c> algaeAquaticEPs{get;set;}

	private List<Endpoint__c> returnAquaticEndpoints(String aquaticType){
		return [select id,
						Toxicity_Value__c,
						Data_Source__c,
						Analog__c,
						Notes__c,
						Test_Type__c,
						Endpoint__c
						from Endpoint__c
						where Aquatic_Toxicity_Type__c = :aquaticType
							and TPP_Chemical_Data__c = :tcd.Id
						Order By CreatedDate ASC];
	}

	private List<Endpoint__c> returnEndpoints(String endpoint){
		return [select id,
						Call__c,
						Exposure_Route__c,
						Toxicity_Value__c,
						Unit__c,
						Acute_Mammalian_Units__c,
						Unit_Other__c,
						Data_Source__c,
						Analog__c,
						Notes__c,
						Data_Summary__c,
						Data_Availability__c,
						Test_Type__c,
						Endpoint__c
						from Endpoint__c
						where Endpoint__c = :endpoint
							and TPP_Chemical_Data__c = :tcd.Id
						Order By CreatedDate ASC];
	}
}